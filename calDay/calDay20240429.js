// 创建于20240429
// 更新于20240503
// 作者：wangc
// 新增：创建基础模板
// 修改：无
// 删除：无

// 通过异步创建桌面组件实例
let widget = await createWidget();

// 开始创建渐变背景
let bgColor = new LinearGradient();
bgColor.locations = [0, 1];
bgColor.colors = [
    new Color("#FFFFE0"),
    new Color("#E1FFFF")
];
// 添加渐变颜色到组件背景
widget.backgroundGradient = bgColor;

// 如果是在app中运行，则显示预览
if(config.runsInApp) {
    await widget.presentMedium();
}
// 添加组件实例
Script.setWidget(widget);
Script.complete();

// 定义异步操作
async function createWidget() {
    let w = new ListWidget();
    const nowDate = new Date();
    console.log(nowDate);
    w.addText("九七今日班次：" + calShift(nowDate));
    w.addText("老爸今日班次：" + calShiftDay(nowDate));
    w.addText("更新时间：" + formatDateTime(nowDate));
    return w;
}
// 计算九七班次
function calShift(dateStr) {
    // 开始日期
    const startDate = new Date('2024-04-02T00:00:00');
     // 当前日期
    const nowDate = new Date(dateStr);
    console.log((nowDate - startDate) / (1000 * 60 * 60 * 24))
    // 计算两个日期之间的天数差
    const daysDiff = Math.floor((nowDate - startDate) / (1000 * 60 * 60 * 24));
    // 根据天数差计算班次索引，使用模运算确保循环
    const shiftIndex = daysDiff % 3;
    // 根据班次索引返回对应的班次
    switch (shiftIndex) {
        case 0:
            return '夜班';
        case 1:
            return '出班';
        default:
            return '休息';
    }
}
// 计算老爸班次
function calShiftDay(dateStr) {
    // 开始日期
    const startDate = new Date('2024-04-08T00:00:00'); 
    // 当前日期
    const nowDate = new Date(dateStr); 
    // 计算两个日期之间的天数差
    const daysDiff = Math.floor((nowDate - startDate) / (1000 * 60 * 60 * 24));
    // 计算班次循环中的位置（0-11），以及在当前班次中的第几天（1-3）
    const cyclePos = daysDiff % 12;
    const dayInShift = cyclePos % 3 + 1;
    // 确保第1天开始计数
//     const dayInShift = ((daysDiff - cyclePos) % 12) / 3 + 1; 
    // 根据班次循环位置返回对应的班次
    let shift;
    switch (Math.floor(cyclePos / 3)) {
        case 0:
            shift = '大夜';
            break;
        case 1:
            shift = '白班';
            break;
        case 2:
            shift = '小夜';
            break;
        default:
            shift = '休息';
    }
    return shift + ' 第' + dayInShift + '天';
}

// 日期格式化yyyy-mm-dd hh:mm:ss
function formatDateTime(date) {
    const year = date.getFullYear()
    const month = (date.getMonth() + 1 + '').padStart(2, '0')
    const day = (date.getDate() + '').padStart(2, '0')
    const hour = (date.getHours() + '').padStart(2, '0')
    const minute = (date.getMinutes() + '').padStart(2, '0')
    const second = (date.getSeconds() + '').padStart(2, '0')
    return `${year}-${month}-${day} ${hour}:${minute}:${second}`
}