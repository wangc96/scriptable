// 创建于20240503
// 更新于20240503
// 作者：wangc
// 新增：无
// 修改：合并相似函数，使函数更具通用性
// 删除：无

// 通过异步创建桌面组件实例
let widget = await createWidget();

// 开始创建渐变背景
let bgColor = new LinearGradient();
bgColor.locations = [0, 1];
bgColor.colors = [
    new Color("#FFFFE0"),
    new Color("#E1FFFF")
];
// 添加渐变颜色到组件背景
widget.backgroundGradient = bgColor;

// 如果是在app中运行，则显示预览
if(config.runsInApp) {
    await widget.presentMedium();
}
// 添加组件实例
Script.setWidget(widget);
Script.complete();

// 定义异步操作
async function createWidget() {
    let w = new ListWidget();
    const nowDate = new Date();
    console.log(nowDate);
    shiftInfo = {
        name: "老爸",
        shiftDay: ["大夜","白班","小夜","休息"],
        shiftDayNum: 3,
        startDate: "2024-04-08T00:00:00",
        endDate: nowDate,
    }
    shiftInfo97 = {
        name: "九七",
        shiftDay: ["夜班","出班","休息"],
        shiftDayNum: 1,
        startDate: "2024-04-02T00:00:00",
        endDate: nowDate,
    }
    w.addText("九七今日班次：" + calShiftNew(shiftInfo97));
    w.addText("老爸今日班次：" + calShiftNew(shiftInfo));
    w.addText("更新时间于：" + formatDateTime(nowDate));
    return w;
}
// 计算班次new
function calShiftNew(shiftInfo) {
    // 开始日期
    const startDate = new Date(shiftInfo.startDate); 
    // 当前日期
    const endDate = new Date(shiftInfo.endDate); 
    // 计算两个日期之间的天数差
    const daysDiff = Math.floor((endDate - startDate) / (1000 * 60 * 60 * 24));
    // 计算班次循环中的位置（0-11），以及在当前班次中的第几天（1-3）
    const cyclePos = daysDiff % (shiftInfo.shiftDay.length * shiftInfo.shiftDayNum)
    const dayInShift = cyclePos % shiftInfo.shiftDayNum + 1;
    // 根据班次循环位置返回对应的班次
    let shift = shiftInfo.shiftDay[Math.floor(cyclePos / shiftInfo.shiftDayNum)];  
    return shift + ' 第' + dayInShift + '天';
}
// 日期格式化yyyy-mm-dd hh:mm:ss
function formatDateTime(date) {
    const year = date.getFullYear()
    const month = (date.getMonth() + 1 + '').padStart(2, '0')
    const day = (date.getDate() + '').padStart(2, '0')
    const hour = (date.getHours() + '').padStart(2, '0')
    const minute = (date.getMinutes() + '').padStart(2, '0')
    const second = (date.getSeconds() + '').padStart(2, '0')
    return `${year}-${month}-${day} ${hour}:${minute}:${second}`
}