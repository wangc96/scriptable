// 通过异步创建桌面组件实例
let widget = await createWidget();

// 如果是在app中运行，则显示预览
if(config.runsInApp) {
    await widget.presentMedium();
}
// 添加组件实例
Script.setWidget(widget);
Script.complete();

// 定义异步操作
async function createWidget() {
    let w = new ListWidget();
    const nowDate = new Date();
    let year = nowDate.getFullYear();
    let month = ("0" + (nowDate.getMonth() + 1)).slice(-2); // 月份从0开始计数，所以加1
    let day = ("0" + nowDate.getDate()).slice(-2);

    const nowDateSlice = `${year}-${month}-${day}`;
    const url = 'http://timor.tech/api/holiday/next/' + nowDateSlice +'?type=Y&week=Y';
    console.log(url);
    let jsonData = await httpGet(url);
    console.log(jsonData);
    // 解析JSON字符串为JavaScript对象

    // 访问date的值
    let dateValue = jsonData.holiday.date;
    console.log(dateValue); // 输出: 2024-05-01

    w.addText(dateValue);
    return w;
}

// GET函数封装
async function httpGet(url) {
    fetch(url, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        }
    })
    .then(response => response.json())
    .then(data => console.log(data))
    .catch(error => console.error('Error:', error));

    return data;
}

// GET函数封装
// async function httpGet(url) {
//     let req = new Request(url);
//     let data = await req.loadJSON();
//     return data;
// }

